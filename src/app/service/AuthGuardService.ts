import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UsuarioService } from './usuario.service';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private userService: UsuarioService, private router: Router) {}
  
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    console.log(route);
    console.log(state.url);
    
    if (state.url==="/nueva_consulta/formulario"&&sessionStorage.getItem('pacienteNombre')!=null) {
      return true;
    }
    if(state.url==="/nueva_consulta/formulario"&&sessionStorage.getItem("userActual")!=null){
      this.router.navigate(['nueva_consulta']);
      return false;
    }

    if (sessionStorage.getItem("userActual")!=null) {
      return true;
    } else {
    this.router.navigate(['']);
      return false;
    }

  }
}
