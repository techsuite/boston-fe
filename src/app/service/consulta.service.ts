import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Consulta } from '../model/Consulta/consulta';
import { ConsultaMetadata } from '../model/Consulta/consultaMetadata';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class ConsultaService {
  pageNum: number = 1;
  consultas: ConsultaMetadata;
  pageSize: number = 5;

  getPageSize(): number {
    return this.pageSize;
  }
  
  getPageNum() {
    return this.pageNum;
  }

  setPageNum(num: number) {
    this.pageNum = num;
    sessionStorage.setItem('paginaActualCon', this.pageNum.toString()); //guardo en memoria la pagina en la que me encuentro para poder recargar y seguir avanzando bien
  }

  getCons() {
    return this.consultas;
  }
  setCons(cons: ConsultaMetadata) {
    this.consultas = cons;
  }

  constructor(private api: ApiService) {}

  getConsultasByDNI(dni: string, pageNumber: number, pageSize: number): Observable<ConsultaMetadata> {
    return this.api.get(`consultasDNI/${dni}/pagina?pageNumber=${pageNumber}&pageSize=${pageSize}`);
  }
  saveConsulta(consulta: Consulta): Observable<void>{
    return this.api.post(`addConsulta`, consulta);
  }
}
