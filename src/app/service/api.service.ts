import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ExceptionService } from './exception.service';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  readonly SERVER = 'https://boston-be.juliocastrodev.duckdns.org';

  constructor(private http: HttpClient,private exceptionService: ExceptionService) {}

  get<T>(endpoint: string): Observable<T> {
    return this.execute('GET', endpoint);
    //return this.http.get<T>(`${this.SERVER}/${endpoint}`);
  }

  post<T>(endpoint: string, body: any): Observable<T> {
    //return this.execute('POST', endpoint);

    return this.http.post<T>(`${this.SERVER}/${endpoint}`, body);
  }

  // Cuando haya error se va a meter por la rama de errores y va a devolver un null
  private execute<T>(method: "GET" | "POST", endpoint: string, body?: any): Observable<T>{
    return this.http.request<T>(method, `${this.SERVER}/${endpoint}`, {body})
      .pipe(
        catchError((error: HttpErrorResponse) => {
          this.exceptionService.handleHttpError(error);
        return of(null); // Le llega al usuario un null
      }));
  }

}
