import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Paciente } from '../model/Paciente/paciente';
import { PacienteMetadata } from '../model/Paciente/pacienteMetadata';
import { Usuario } from '../model/usuario';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class PacienteService {
  pageNum: number = 1;
  pacientes: PacienteMetadata;
  pageSize: number = 10;

  getPageSize(): number {
    return this.pageSize;
  }
  
  getPageNum() {
    return this.pageNum;
  }

  setPageNum(num: number) {
    this.pageNum = num;
    sessionStorage.setItem('paginaActual', this.pageNum.toString()); //guardo en memoria la pagina en la que me encuentro para poder recargar y seguir avanzando bien
  }


  getPacs() {
    return this.pacientes;
  }
  setPacs(pacs: PacienteMetadata) {
    this.pacientes = pacs;
  }

  //aqui guardo el nombre del paciente para poder recuperarlo en pagina siguiente
  nombrePaciente: string;
  dniPaciente: string;
  

  setDniPaciente(dni: string) {
    this.dniPaciente = dni;
  }
  getDniPaciente(): string {
    return this.dniPaciente;
  }

  setNombrePaciente(nombre: string) {
    this.nombrePaciente = nombre;
  }
  getNombrePaciente(): string {
    return this.nombrePaciente;
  }
  
  constructor(private api: ApiService) {}

  getPacientes(dni: string, dniPac: string, pageNumber: number, pageSize: number): Observable<PacienteMetadata> {
    return this.api.get(`getPacientes/${dni}/${dniPac}/pagina?pageNumber=${pageNumber}&pageSize=${pageSize}`);
  }

  getNoPacientes(dni: string, dniPac: string, pageNumber: number, pageSize: number): Observable<PacienteMetadata> {
    return this.api.get(`getNoPacientes/${dni}/${dniPac}/pagina?pageNumber=${pageNumber}&pageSize=${pageSize}`);
  }

  anadirPaciente(paciente: Paciente): Observable<void>{
    return this.api.post(`insertarNuevo`, paciente);
  }

  asociarPaciente(dniUsuario: string, dniPaciente: string): Observable<void> {
    return this.api.post(`asociarPaciente`, {'dniUsuario':dniUsuario, 'dniPaciente': dniPaciente});
  }

  actualizarFechaConsulta(dni: string, fechaUltimaConsulta: string): Observable<void>{
    return this.api.post(`actualizarFechaConsulta`, {'dniPaciente':dni, 'fecha':fechaUltimaConsulta});
  }

}