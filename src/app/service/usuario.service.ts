import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Usuario } from '../model/usuario';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  
  constructor(private api: ApiService) {}

  getUsuario(dni: string): Observable<Usuario> {
    return this.api.get(`user/${dni}`);
  }
  verificacion(dni: string, contrasena: string): Observable<Boolean>{
    return this.api.get(`check/${dni}/${contrasena}/`)
  }


}
