export class Usuario {
    dni: string;
    nombre: string;
    apellidos: string;
    email: string;
    contrasena: string;
    fecha_de_nacimiento: Date;
    genero: string;
  }