export class Paciente {
	dni: string;
	nombre: string;
	apellido: string;
	email: string;
	fechaRegistro: string;
	fechaUltimaConsulta: string;
	fechaNacimiento: string;
	genero: string;

	constructor(){
	}
}