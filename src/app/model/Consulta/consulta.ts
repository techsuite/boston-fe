export class Consulta {
	idConsulta: number;
	fechaRegistroPaciente: string;
	fechaConsulta: string;
	enfermedadesDiagnosticadas: string;
	medicamentosRecetados: string;
	pruebasDeLaboratorioARealizar: string;
    observaciones: string;
	dniDoctor: string;
	nombreDoctor: string;
	apellidosDoctor: string;
	fechaNacimientoDoctor: string;
	correoElectronicoDoctor: string;
	generoDoctor: string;
	passwordDoctor: string;
	dniPaciente: string;
	nombrePaciente: string;
	apellidosPaciente: string;
	fechaNacimientoPaciente: string;
	correoElectronicoPaciente: string;
	generoPaciente: string;

constructor(){}

}