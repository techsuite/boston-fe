import { Consulta } from "./consulta";



export class ConsultaMetadata{

    "consulta": Consulta[];

    "infoPagina": {
        "pageSize": number,
        "pageNumber": number,
        "totalPages": number
    }

}