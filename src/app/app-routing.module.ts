import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormularioConsultaPageComponent } from './pages/formulario_consulta/formulario_consulta.page.component';
import { FormularioPacientePageComponent } from './pages/formulario_paciente/formulario_paciente.page.component';
import { HomePageComponent } from './pages/home/home.page.component';
import { LoginPageComponent } from './pages/login/login.page.component';
import { NuevaConsultaPageComponent } from './pages/nueva_consulta/nueva_consulta.page.component';
import { NuevoPacientePageComponent } from './pages/nuevo_paciente/nuevo_paciente.page.component';
import { AuthGuardService } from './service/AuthGuardService';
const routes: Routes = [
  { path: 'login', component: LoginPageComponent},
  { path: 'home', component: HomePageComponent,canActivate:[AuthGuardService]},
  { path: 'nueva_consulta', component: NuevaConsultaPageComponent,canActivate:[AuthGuardService]},
  { path: 'nuevo_paciente', component: NuevoPacientePageComponent,canActivate:[AuthGuardService]},
  { path: 'nueva_consulta/formulario', component: FormularioConsultaPageComponent, canActivate:[AuthGuardService]},
  { path: 'nuevo_paciente/formulario', component: FormularioPacientePageComponent, canActivate:[AuthGuardService]},
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: '**', redirectTo: 'nueva_consulta', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers:[AuthGuardService],
})
export class AppRoutingModule {}
