import { HttpClient, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { Consulta } from 'src/app/model/Consulta/consulta';
import { ConsultaMetadata } from 'src/app/model/Consulta/consultaMetadata';
import { Paciente } from 'src/app/model/Paciente/paciente';
import { PacienteMetadata } from 'src/app/model/Paciente/pacienteMetadata';
import { Usuario } from 'src/app/model/usuario';
import { ConsultaService } from 'src/app/service/consulta.service';
import { PacienteService } from 'src/app/service/paciente.service';
import { UsuarioService } from 'src/app/service/usuario.service';

import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  templateUrl: './nueva_consulta.page.component.html',
  styleUrls: ['./nueva_consulta.page.component.scss'],
})
export class NuevaConsultaPageComponent{

  dniPaciente: string;
  dniPac: string;
  nombrePaciente: string;
  apellidosPaciente: string;
  dni="";
  pacientesAso: PacienteMetadata = this.pacienteService.getPacs();
  consultasPre: ConsultaMetadata = this.consultaService.getCons();
  constructor(private usuarioService: UsuarioService,public router: Router,
    private consultaService: ConsultaService, private pacienteService: PacienteService) { }

  
  ngOnInit(): void {
    this.pacienteService.setPageNum(1);
    this.recargarPagPac();
  }

  pacientesDisponibles() {

    if (this.pacienteService.getPacs() !== null) {
      this.pacientesAso = this.pacienteService.getPacs();
    }
    return this.pacientesAso !== null;
  }

  pacientesAsociados(dniDoctor: string): void {
    if(this.dni == "")
      this.dniPac=" ";
    else
      this.dniPac=this.dni;
    this.pacienteService.getPacientes(dniDoctor,this.dniPac,1, 2)
      .subscribe((resp: PacienteMetadata) => {
        this.pacienteService.setPageNum(1);
        this.pacientesAso = resp;
        this.pacienteService.setPacs(resp);
        sessionStorage.setItem('pacientesAsociados', JSON.stringify(resp)); //guardo en memoria
      });
  }

  sigPagPac(): void {
    if (this.masPaginasDchaPac()) {
      this.pacienteService.setPageNum(this.pacienteService.getPageNum() + 1);
      this.recargarPagPac();
    }
  }
  antPagPac(): void {
    if (this.masPaginasIzqPac()) {
      this.pacienteService.setPageNum(this.pacienteService.getPageNum()- 1);
      this.recargarPagPac();
    }
  }
  masPaginasDchaPac(): boolean {
    return this.pacientesAso?.infoPagina?.pageNumber != this.pacientesAso?.infoPagina?.totalPages;
  }

  masPaginasIzqPac(): boolean {
    return this.pacientesAso?.infoPagina?.pageNumber != 1;
  }

  recargarPagPac() {
    if(this.dni == "")
      this.dniPac=" ";
    else
      this.dniPac=this.dni;

      this.pacienteService
        .getPacientes(sessionStorage.getItem("userActual"),this.dniPac,this.pacienteService.getPageNum(),this.pacienteService.getPageSize())
        .subscribe((resp: PacienteMetadata) => {
          this.pacientesAso = resp;
          this.pacienteService.setPacs(resp);
          sessionStorage.setItem('pacientesAsociados', JSON.stringify(resp)); //guardo en memoria
        });
  }

  mostrarConsulta(dniPaciente: string, nombrePaciente:string, apellidosPaciente:string,  fechaUltimaconsulta: Date){
    var tablaConsulta = document.getElementById('consultas-previas-paciente');
    if(fechaUltimaconsulta!==null){
      if(this.consultasDisponibles()){
        document.getElementById('noConsultasPrevias').style.display = 'none';
        document.getElementById('botonesConsultasPrevias').style.display = 'block';
        tablaConsulta.style.display = 'block';
        document.getElementById('consPrevias').style.display = 'block';
        sessionStorage.setItem('pacienteNombre',nombrePaciente);
        sessionStorage.setItem('pacienteApellido',apellidosPaciente);
        sessionStorage.setItem('pacienteConsulta',dniPaciente);
        this.nombrePaciente= nombrePaciente;
        this.apellidosPaciente=apellidosPaciente;
        this.dniPaciente = dniPaciente.toUpperCase();
        this.recargarPagCon();
      }
    }
    else{
      tablaConsulta.style.display = 'block';
      document.getElementById('consPrevias').style.display = 'none';
      document.getElementById('botonesConsultasPrevias').style.display = 'none';
      document.getElementById('noConsultasPrevias').style.display = 'block';
      sessionStorage.setItem('pacienteNombre',nombrePaciente);
      sessionStorage.setItem('pacienteApellido',apellidosPaciente);
      sessionStorage.setItem('pacienteConsulta',dniPaciente);
      this.nombrePaciente= nombrePaciente;
      this.apellidosPaciente= apellidosPaciente;
      this.dniPaciente = dniPaciente.toUpperCase();
    }
    
  }
  

  consultasDisponibles() {
    if (this.consultaService.getCons() !== null) {
      this.consultasPre = this.consultaService.getCons();
    }
    return this.consultasPre !== null;
  }

  consultasPrevias(dniPaciente: string): void {
    console.log("consultasPRevias");
    this.consultaService.getConsultasByDNI(dniPaciente,1,1)
      .subscribe((resp: ConsultaMetadata) => {
        this.consultaService.setPageNum(1);
        this.consultasPre = resp;
        this.consultaService.setCons(resp);
        sessionStorage.setItem('consultasPrevias', JSON.stringify(resp)); //guardo en memoria
        
      });
  }

  sigPagCon(): void {
    if (this.masPaginasDchaCon()) {
      this.consultaService.setPageNum(this.consultaService.getPageNum() + 1);
      this.recargarPagCon();
    }
  }
  antPagCon(): void {
    if (this.masPaginasIzqCon()) {
      this.consultaService.setPageNum(this.consultaService.getPageNum()- 1);
      this.recargarPagCon();
    }
  }
  masPaginasDchaCon(): boolean {
    return this.consultasPre?.infoPagina?.pageNumber != this.consultasPre?.infoPagina?.totalPages;
  }

  masPaginasIzqCon(): boolean {
    return this.consultasPre?.infoPagina?.pageNumber != 1;
  }

  recargarPagCon() {
      this.consultaService
        .getConsultasByDNI(sessionStorage.getItem("pacienteConsulta"), this.consultaService.getPageNum(),this.consultaService.getPageSize())
        .subscribe((resp: ConsultaMetadata) => {
          this.consultasPre = resp;
          this.consultaService.setCons(resp);
          sessionStorage.setItem('consultasPrevias', JSON.stringify(resp)); //guardo en memoria
        });
  }

  consultaNueva(){
    this.router.navigate(['nueva_consulta/formulario']);
  }
  logout(){
		Swal.fire({
      title: '¿Quiere cerrar sesión?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sí, adelante',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {
        sessionStorage.removeItem("userActual");
		    sessionStorage.clear();
		    this.router.navigate(['/']);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelado',
          'Ha cancelado la acción',
          'error'
        )
      }
    })
	}
  buscar(){    
    this.pacienteService.setPageNum(1);
    this.recargarPagPac();
  }
}

