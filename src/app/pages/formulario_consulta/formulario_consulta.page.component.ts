import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Consulta } from "src/app/model/Consulta/consulta";
import { ConsultaService } from "src/app/service/consulta.service";
import { PacienteService } from "src/app/service/paciente.service";
import { AyudaConsultaPopUpComponent} from "src/app/components/ayuda-consulta-pop-up/ayuda-consulta-pop-up.component"
import { MatDialog } from "@angular/material/dialog";
import Swal from 'sweetalert2/dist/sweetalert2.js';


@Component({
    templateUrl: './formulario_consulta.page.component.html',
    styleUrls: ['./formulario_consulta.page.component.scss'],
  })
  export class FormularioConsultaPageComponent implements OnInit {

    consulta: Consulta = new Consulta();
    dniPac: string= sessionStorage.getItem('pacienteConsulta');
    nomPac: string= sessionStorage.getItem('pacienteNombre');
    apePac: string= sessionStorage.getItem('pacienteApellido');
    fecha: string=new Date().toISOString().slice(0, 10);
    constructor(private route: Router,private consultaService: ConsultaService,
    private pacienteService: PacienteService, private popUpService: MatDialog){
    }
    ngOnInit(): void {
        
    }
    
    submit() {
      Swal.fire({
        title: '¿Informe completado?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Sí, adelante',
        cancelButtonText: 'No'
      }).then((result) => {
        if (result.value) {
          Swal.fire(
            '¡Hecho!',
            'Informe añadido',
            'success'
          );
          this.consulta.fechaConsulta = this.fecha;
          this.consulta.dniDoctor=sessionStorage.getItem('userActual');
          this.consulta.dniPaciente=this.dniPac;
          this.consultaService.saveConsulta(this.consulta).subscribe();
      
          this.pacienteService.actualizarFechaConsulta(this.dniPac,this.consulta.fechaConsulta).subscribe();
          setTimeout(()=>{this.route.navigate(['/nueva_consulta']);},1000);
        }
          
        else if (result.dismiss === Swal.DismissReason.cancel) {
          Swal.fire(
            'Cancelado',
            'Ha cancelado la acción',
            'error'
          )
        }
      })
    }

    volver(){
      this.route.navigate(['/nueva_consulta']);
    }
    ayuda(){
      this.popUpService.open(AyudaConsultaPopUpComponent);
    }
    isInvalid() {
      return !this.consulta.enfermedadesDiagnosticadas || !this.consulta.observaciones
      || !this.consulta.pruebasDeLaboratorioARealizar || !this.consulta.medicamentosRecetados;
    }
  } 