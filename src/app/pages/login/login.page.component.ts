
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/service/usuario.service';

export class Datos{
  dni: string;
  contrasena: string;
}

@Component({
  selector: "app-login",
  templateUrl: './login.page.component.html',
  styleUrls: ['./login.page.component.scss'],
})
export class LoginPageComponent{
  datos: Datos = new Datos();
 
  constructor(public usuarioService: UsuarioService, public router: Router){}
 
  login(){
    var regexDNI = new RegExp('^\\d{8}[a-zA-Z]$');
   //Verificamos que el DNI tenga un formato valido
   var errDNI = document.getElementById('error-dni');
   var errUsrContr = document.getElementById('error');
   if (!regexDNI.test(this.datos.dni)) {
     errDNI.style.display = 'block';
     errUsrContr.style.display = 'none';
   } else {
     errDNI.style.display = 'none';
     //Si lo tiene, comprobamos que el usuario-contrasena es correcto
     this.usuarioService.verificacion(this.datos.dni, this.datos.contrasena)
     .subscribe((bool)=>{
       if(bool){
         sessionStorage.setItem("userActual", this.datos.dni);
         this.router.navigate(['/home']);
       }else{
         errUsrContr.style.display = 'block';
       }
     });
   }

  }
  

}