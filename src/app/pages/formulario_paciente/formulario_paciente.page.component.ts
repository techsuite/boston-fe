
import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Router } from "@angular/router";
import { AyudaPacientePopUpComponent } from "src/app/components/paciente-ayuda-pop-up/ayuda-paciente-pop-up.component";
import { Paciente } from "src/app/model/Paciente/paciente";
import { PacienteService } from "src/app/service/paciente.service";
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
    templateUrl: './formulario_paciente.page.component.html',
    styleUrls: ['./formulario_paciente.page.component.scss'],
  })
  export class FormularioPacientePageComponent implements OnInit {

    dniUsuario: string= sessionStorage.getItem('userActual');
    paciente: Paciente = new Paciente();


    constructor( private route: Router,private pacienteService: PacienteService,
      private popUpService: MatDialog){
    }
    ngOnInit(): void {
        
    }
    
    submit() {
      Swal.fire({
        title: '¿Registrar nuevo paciente?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Sí, adelante',
        cancelButtonText: 'No'
      }).then((result) => {
        if (result.value) {
          Swal.fire(
            '¡Hecho!',
            'Paciente registrado',
            'success'
          );
          this.paciente.fechaRegistro= new Date().toISOString().slice(0, 10);
          this.pacienteService.anadirPaciente(this.paciente).subscribe(()=>{
            this.pacienteService.asociarPaciente(sessionStorage.getItem('userActual'),this.paciente.dni).subscribe();
          });
          this.pacienteService.setPageNum(1);
          setTimeout(()=>{this.route.navigate(['/nueva_consulta']);},1000);
        }
          
        else if (result.dismiss === Swal.DismissReason.cancel) {
          Swal.fire(
            'Cancelado',
            'Ha cancelado la acción',
            'error'
          )
        }
      })
      

      

    }

    volver(){
      this.route.navigate(['/nuevo_paciente']);
    }

    isInvalid() {
      var regexDNI = new RegExp('^\\d{8}[a-zA-Z]$');

      return !this.paciente.dni || !this.paciente.nombre || ! this.paciente.email
      || !this.paciente.apellido || !this.paciente.genero || !this.paciente.fechaNacimiento ||
      !regexDNI.test(this.paciente.dni);

    }
    ayuda(){
      this.popUpService.open(AyudaPacientePopUpComponent);
    }
  }
