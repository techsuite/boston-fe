import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from 'src/app/model/usuario'
import { UsuarioService } from 'src/app/service/usuario.service';


@Component({
  templateUrl: './home.page.component.html',
  styleUrls: ['./home.page.component.scss'],
})
export class HomePageComponent implements OnInit {

	usuario: Usuario;
	controlador: Boolean = false;
	constructor(private usuarioService:UsuarioService, private router: Router) { }
	
	ngOnInit(): void{
		this.usuarioService.getUsuario(sessionStorage.getItem("userActual"))
		.subscribe((user) => {this.usuario=user;

		if(this.usuario.genero=="femenino"){
			document.getElementById('home-page-title2').style.display='block';
		}else{
			document.getElementById('home-page-title').style.display='block';
		}});
		this.cuentaAtras();
	}

	logout(){
		sessionStorage.removeItem("userActual");
		sessionStorage.clear();
		this.router.navigate(['/']);
	}

	continuar(){
		this.controlador = true;
		this.router.navigate(['nueva_consulta']);
	}
	


	cuentaAtras(){
		var count = 4;
		document.getElementById('countdown').innerHTML = String(4);
		var contador=setInterval(()=>{
			count--;
			if(count == 0 || this.controlador){
				this.continuar();
				clearInterval(contador);
			}
			document.getElementById('countdown').innerHTML = String(count);
		},1000);
		
		
	}
	
	
}
