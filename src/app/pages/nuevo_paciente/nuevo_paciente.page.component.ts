import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PacienteComponent } from 'src/app/components/paciente/paciente.component';
import { UsuarioComponent } from 'src/app/components/usuario/usuario.component';
import { PacienteMetadata } from 'src/app/model/Paciente/pacienteMetadata';
import { PacienteService } from 'src/app/service/paciente.service';
import { UsuarioService } from 'src/app/service/usuario.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  templateUrl: './nuevo_paciente.page.component.html',
  styleUrls: ['./nuevo_paciente.page.component.scss'],
})
export class NuevoPacientePageComponent{
	
  dniPaciente: string;
  dniPac: string;
  dni="";
  usuarioComponent: UsuarioComponent;
  pacienteComponent: PacienteComponent;
  pacientesNoAso: PacienteMetadata = this.pacienteService.getPacs();
  constructor(private usuarioService: UsuarioService,private route: Router,
    private pacienteService: PacienteService) { }

  
  ngOnInit(): void {
    this.pacienteService.setPageNum(1);
    this.recargarPagNoPac();
    
  }

  pacientesDisponibles() {
    if (this.pacienteService.getPacs() !== null) {
      this.pacientesNoAso = this.pacienteService.getPacs();
    }
    return this.pacientesNoAso !== null;
  }

  pacientesNoAsociados(dniDoctor: string): void {
    if(this.dni == "")
      this.dniPac=" ";
    else
      this.dniPac=this.dni;

    this.pacienteService.getNoPacientes(dniDoctor,this.dniPac,1, 2)
      .subscribe((resp: PacienteMetadata) => {
        this.pacienteService.setPageNum(1);
        this.pacientesNoAso = resp;
        this.pacienteService.setPacs(resp);
        sessionStorage.setItem('pacientesNoAsociados', JSON.stringify(resp)); //guardo en memoria
      });
  }

  sigPagNoPac(): void {
    if (this.masPaginasDchaNoPac()) {
      this.pacienteService.setPageNum(this.pacienteService.getPageNum() + 1);
      this.recargarPagNoPac();
    }
  }
  antPagNoPac(): void {
    if (this.masPaginasIzqNoPac()) {
      this.pacienteService.setPageNum(this.pacienteService.getPageNum()- 1);
      this.recargarPagNoPac();
    }
  }
  masPaginasDchaNoPac(): boolean {
    return this.pacientesNoAso?.infoPagina?.pageNumber != this.pacientesNoAso?.infoPagina?.totalPages;
  }

  masPaginasIzqNoPac(): boolean {
    return this.pacientesNoAso?.infoPagina?.pageNumber != 1;
  }

  recargarPagNoPac() {
    if(this.dni == "")
      this.dniPac=" ";
    else
      this.dniPac=this.dni;

      this.pacienteService
        .getNoPacientes(sessionStorage.getItem('userActual'), this.dniPac, this.pacienteService.getPageNum(),this.pacienteService.getPageSize())
        .subscribe((resp: PacienteMetadata) => {
          this.pacientesNoAso = resp;
          this.pacienteService.setPacs(resp);
          sessionStorage.setItem('pacientesNoAsociados', JSON.stringify(resp)); //guardo en memoria
        });
  }
  
  asociarPac(dniPaciente: string){
    Swal.fire({
      title: '¿Quiere añadir este paciente?',
      text: 'La acción es irreversible.',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sí, adelante',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {
        sessionStorage.setItem('pacienteRelacion',dniPaciente);
        
        Swal.fire(
          '¡Hecho!',
          'Acción realizada con éxito',
          'success'
        );
        this.pacienteService.asociarPaciente(sessionStorage.getItem('userActual'), dniPaciente)
        .subscribe(()=>{
        window.location.reload();
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelado',
          'Ha cancelado la acción',
          'error'
        )
      }
    })
  }  

  pacienteNuevo(){
    this.route.navigate(['/nuevo_paciente/formulario']);
  }

  logout(){
    Swal.fire({
      title: '¿Quiere cerrar sesión?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sí, adelante',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {
        sessionStorage.removeItem("userActual");
		    sessionStorage.clear();
		    this.route.navigate(['/']);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelado',
          'Ha cancelado la acción',
          'error'
        )
      }
    })
		
	}
  buscar(){    
    this.pacienteService.setPageNum(1);
    this.recargarPagNoPac();    
  }
}
