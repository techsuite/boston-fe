import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { HttpClientModule } from '@angular/common/http';
import { HomePageComponent } from './pages/home/home.page.component';
import { CenterLayoutComponent } from './layouts/center/center.layout.component';
import { NavBarElementComponent } from './components/nav-bar/nav-bar-element/nav-bar-element.component';
import { UsuarioComponent } from './components/usuario/usuario.component';
import { LoginPageComponent } from './pages/login/login.page.component';
import { NuevaConsultaPageComponent } from './pages/nueva_consulta/nueva_consulta.page.component';
import { NuevoPacientePageComponent } from './pages/nuevo_paciente/nuevo_paciente.page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PacienteComponent } from './components/paciente/paciente.component';
import { ConsultaComponent } from './components/consulta/consulta.component';
import { FilterPipe } from './pipes/filter.pipe';
import { MatDialogModule} from '@angular/material/dialog';
import { FormularioConsultaPageComponent } from './pages/formulario_consulta/formulario_consulta.page.component';
import { FormularioPacientePageComponent } from './pages/formulario_paciente/formulario_paciente.page.component';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { RequiredInputComponent } from './components/required-input/required-input.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule } from '@angular/material/slider';
import { AyudaConsultaPopUpComponent } from './components/ayuda-consulta-pop-up/ayuda-consulta-pop-up.component';
import { AyudaPacientePopUpComponent } from './components/paciente-ayuda-pop-up/ayuda-paciente-pop-up.component';
import { MatDatepickerModule } from '@angular/material/datepicker'
import { MatNativeDateModule, MatRippleModule, MAT_DATE_LOCALE } from '@angular/material/core';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    HomePageComponent,
    CenterLayoutComponent,
    NavBarElementComponent,
    UsuarioComponent,
    PacienteComponent,
    ConsultaComponent,
    LoginPageComponent,
    NuevaConsultaPageComponent,
    NuevoPacientePageComponent,
    FilterPipe,
    FormularioConsultaPageComponent,
    FormularioPacientePageComponent,
    RequiredInputComponent,
    AyudaConsultaPopUpComponent,
    AyudaPacientePopUpComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, 
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatCardModule,
    MatFormFieldModule,  
    MatInputModule,  
    MatSelectModule, 
    BrowserAnimationsModule,
    MatSliderModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRippleModule],
  providers: [{provide: MAT_DATE_LOCALE, useValue: 'en-CA'}],
  bootstrap: [AppComponent],
})
export class AppModule {}
