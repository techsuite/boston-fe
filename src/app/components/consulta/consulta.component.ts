import { Component, Input, OnInit } from '@angular/core';
import { Consulta } from 'src/app/model/Consulta/consulta';
import { ConsultaService } from 'src/app/service/consulta.service';

@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.scss']
})
export class ConsultaComponent implements OnInit {

  @Input() dniPaciente: string;
  consulta: Consulta;

  constructor(private consultaService: ConsultaService) {}

  ngOnInit(): void {
    
  }
 
}