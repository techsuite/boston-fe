import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'pacienteDniFilter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (!args) {
      return value;
    }
    return value.filter((val) => {
      let rVal = (val.dni.toLocaleLowerCase().includes(args));
      return rVal;
    })

  }

}